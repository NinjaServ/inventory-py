import sys
from PyQt4.QtCore import pyqtSlot
from PyQt4.QtGui import *


def main():
    # create our window
    app     = QApplication(sys.argv)
    # The QWidget widget is the base class of all user interface objects in PyQt4.
    w = QMainWindow()
    # Set window title  
    w.setWindowTitle("Menu + Buttons")
    # Set window size. 
    w.resize(500, 250)
    
    table   = QTableWidget(w)
    tableItem = QTableWidgetItem()
 
    # initiate table
    table.setWindowTitle("QTableWidget Example @pythonspot.com")
    table.resize(400, 250)
    table.setRowCount(4)
    table.setColumnCount(2)
 
    # set data
    table.setItem(0,0, QTableWidgetItem("Item (1,1)"))
    table.setItem(0,1, QTableWidgetItem("Item (1,2)"))
    table.setItem(1,0, QTableWidgetItem("Item (2,1)"))
    table.setItem(1,1, QTableWidgetItem("Item (2,2)"))
    table.setItem(2,0, QTableWidgetItem("Item (3,1)"))
    table.setItem(2,1, QTableWidgetItem("Item (3,2)"))
    table.setItem(3,0, QTableWidgetItem("Item (4,1)"))
    table.setItem(3,1, QTableWidgetItem("Item (4,2)"))
 
    # show table
    table.show()

    

# Create main menu
    mainMenu = w.menuBar()
    mainMenu.setNativeMenuBar(False)
    fileMenu = mainMenu.addMenu('File')  #&amp;
 
# Add exit button  
    exitButton = QAction(QIcon('exit24.png'), 'Exit', w)
    exitButton.setShortcut('Ctrl+Q')
    exitButton.setStatusTip('Exit application')
    exitButton.triggered.connect(w.close)
    fileMenu.addAction(exitButton)

 # Create a button in the window
    btn = QPushButton('Click me', w)
    btn.setToolTip('Click to interact!')
#btn.clicked.connect(exit)
#btn.resize(btn.sizeHint())
    btn.move(10, 200)
 
 
# Create the actions
    @pyqtSlot()
    def on_click():
        print('clicked')
        table.setItem(0,0, QTableWidgetItem("Clicked Button"))
 
    @pyqtSlot()
    def on_press():
        print('pressed')
        table.setItem(1,0, QTableWidgetItem("Pressed Button"))

    @pyqtSlot()
    def on_release():
        print('released')
        table.setItem(2,0, QTableWidgetItem("Released Button"))

 
# connect the signals to the slots
    btn.clicked.connect(on_click)
    btn.pressed.connect(on_press)
    btn.released.connect(on_release)
 
# Show the window and run the app
    w.show()
#app.exec_()
    return sys.exit(app.exec_())


if __name__ == '__main__':
    main()
 



 
 


