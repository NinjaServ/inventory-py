# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\python35_win32\Projects\QT-UI\ChemQtr.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(796, 827)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 30, 291, 221))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(50, 190, 201, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Times New Roman"))
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.calendarWidget_2 = QtGui.QCalendarWidget(self.groupBox)
        self.calendarWidget_2.setGeometry(QtCore.QRect(20, 20, 251, 161))
        self.calendarWidget_2.setObjectName(_fromUtf8("calendarWidget_2"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 291, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(500, 30, 291, 221))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.calendarWidget_3 = QtGui.QCalendarWidget(self.groupBox_2)
        self.calendarWidget_3.setGeometry(QtCore.QRect(20, 20, 251, 161))
        self.calendarWidget_3.setObjectName(_fromUtf8("calendarWidget_3"))
        self.label_5 = QtGui.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(40, 190, 201, 20))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Times New Roman"))
        font.setPointSize(14)
        self.label_5.setFont(font)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.tableWidget = QtGui.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 280, 761, 491))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.groupBox_3 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(310, 30, 181, 221))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.pushButton = QtGui.QPushButton(self.groupBox_3)
        self.pushButton.setGeometry(QtCore.QRect(10, 190, 101, 23))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.comboBox = QtGui.QComboBox(self.groupBox_3)
        self.comboBox.setGeometry(QtCore.QRect(10, 140, 161, 22))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.pushButton_3 = QtGui.QPushButton(self.groupBox_3)
        self.pushButton_3.setGeometry(QtCore.QRect(20, 80, 141, 23))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_2 = QtGui.QPushButton(self.groupBox_3)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 50, 141, 23))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.label_4 = QtGui.QLabel(self.groupBox_3)
        self.label_4.setGeometry(QtCore.QRect(60, 120, 71, 20))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.lineEdit = QtGui.QLineEdit(self.groupBox_3)
        self.lineEdit.setGeometry(QtCore.QRect(120, 190, 51, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.groupBox.raise_()
        self.label.raise_()
        self.groupBox_2.raise_()
        self.tableWidget.raise_()
        self.groupBox_3.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 796, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuChemical_Pull_History = QtGui.QMenu(self.menubar)
        self.menuChemical_Pull_History.setObjectName(_fromUtf8("menuChemical_Pull_History"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuChemical_Pull_History.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.groupBox.setTitle(_translate("MainWindow", "Start Date", None))
        self.label_2.setText(_translate("MainWindow", "TextLabel", None))
        self.label.setText(_translate("MainWindow", "TextLabel", None))
        self.groupBox_2.setTitle(_translate("MainWindow", "End Date", None))
        self.label_5.setText(_translate("MainWindow", "TextLabel", None))
        self.groupBox_3.setTitle(_translate("MainWindow", "Query", None))
        self.pushButton.setText(_translate("MainWindow", "Get History", None))
        self.pushButton_3.setText(_translate("MainWindow", "This Quarter", None))
        self.pushButton_2.setText(_translate("MainWindow", "Last Quarter", None))
        self.label_4.setText(_translate("MainWindow", "Pick Chemical", None))
        self.menuChemical_Pull_History.setTitle(_translate("MainWindow", "Chemical Pull History", None))

