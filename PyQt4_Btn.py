import sys
from PyQt4.QtCore import pyqtSlot
from PyQt4.QtGui import *
 
# create our window
app = QApplication(sys.argv)
# w = QWidget()
# w.setWindowTitle('Button click example @pythonspot.com')

# The QWidget widget is the base class of all user interface objects in PyQt4.
w = QMainWindow()
 
# Set window size. 
w.resize(320, 240)
 
# Set window title  
w.setWindowTitle("Menu + Buttons") 


# Create main menu
mainMenu = w.menuBar()
mainMenu.setNativeMenuBar(False)
fileMenu = mainMenu.addMenu('File')  #&amp;
 
# Add exit button  
exitButton = QAction(QIcon('exit24.png'), 'Exit', w)
exitButton.setShortcut('Ctrl+Q')
exitButton.setStatusTip('Exit application')
exitButton.triggered.connect(w.close)
fileMenu.addAction(exitButton)

 
# Create a button in the window
btn = QPushButton('Click me', w)
btn.setToolTip('Click to interact!')
#btn.clicked.connect(exit)
#btn.resize(btn.sizeHint())
btn.move(100, 80)
 
 
# Create the actions
@pyqtSlot()
def on_click():
    print('clicked')
 
@pyqtSlot()
def on_press():
    print('pressed')
 
@pyqtSlot()
def on_release():
    print('released')
 
# connect the signals to the slots
btn.clicked.connect(on_click)
btn.pressed.connect(on_press)
btn.released.connect(on_release)
 
# Show the window and run the app
w.show()
#app.exec_()
sys.exit(app.exec_())

