import sys
#from PyQt4.QtGui import *
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import pyqtSlot



def main():
    # create our window
    app = QtGui.QApplication(sys.argv)
    # The QWidget widget is the base class of all user interface objects in PyQt4.
    w = QtGui.QMainWindow()
    # Set window title  
    w.setWindowTitle("Menu + Buttons")
    # Set window size. 
    w.resize(500, 250)

    # Create main menu
    mainMenu = w.menuBar()
    mainMenu.setNativeMenuBar(False)
    fileMenu = mainMenu.addMenu('File')  #&amp;
 
# Add exit button  
    exitButton = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', w)
    exitButton.setShortcut('Ctrl+Q')
    exitButton.setStatusTip('Exit application')
    exitButton.triggered.connect(w.close)
    fileMenu.addAction(exitButton)

# Create tabs container
    tabs = QtGui.QTabWidget(w)
    w.setCentralWidget(tabs) 
  
    # Create tabs
    tab1    = QtGui.QWidget()   
    tab2    = QtGui.QWidget()
    tab3    = QtGui.QWidget()
    tab4    = QtGui.QWidget()
 
    # Resize width and height
    tabs.resize(500, 250)
 
    # Add tabs
    tabs.addTab(tab1,"Tab 1")
    tabs.addTab(tab2,"Tab 2")
    tabs.addTab(tab3,"Tab 3")
    tabs.addTab(tab4,"Tab 4")


    #TAB 1------------------------------ 
    # Set layout of first tab
    vBoxlayout = QtGui.QVBoxLayout()
    pushButton1 = QtGui.QPushButton("Start")
    pushButton2 = QtGui.QPushButton("Settings")
    pushButton3 = QtGui.QPushButton("Stop")
    vBoxlayout.addWidget(pushButton1)
    vBoxlayout.addWidget(pushButton2)
    vBoxlayout.addWidget(pushButton3)
    tab1.setLayout(vBoxlayout) 

 
#TAB 2------------------------------

    table   = QtGui.QTableWidget(tab2)  #w
    tableItem = QtGui.QTableWidgetItem()
 
    # initiate table
    table.setWindowTitle("QTableWidget Example @pythonspot.com")
    table.resize(400, 250)
    table.setRowCount(4)
    table.setColumnCount(2)
 
    # set data
    table.setItem(0,0, QtGui.QTableWidgetItem("Item (1,1)"))
    table.setItem(0,1, QtGui.QTableWidgetItem("Item (1,2)"))
    table.setItem(1,0, QtGui.QTableWidgetItem("Item (2,1)"))
    table.setItem(1,1, QtGui.QTableWidgetItem("Item (2,2)"))
    table.setItem(2,0, QtGui.QTableWidgetItem("Item (3,1)"))
    table.setItem(2,1, QtGui.QTableWidgetItem("Item (3,2)"))
    table.setItem(3,0, QtGui.QTableWidgetItem("Item (4,1)"))
    table.setItem(3,1, QtGui.QTableWidgetItem("Item (4,2)"))
 
    # show table
    table.show()


 # Create a button in the window
    btn = QtGui.QPushButton('Click me', tab2)
    btn.setToolTip('Click to interact!')
#btn.clicked.connect(exit)
#btn.resize(btn.sizeHint())
    btn.move(10, 200)

# Create the actions
    @pyqtSlot()
    def on_click():
        print('clicked')
        table.setItem(0,0, QTableWidgetItem("Clicked Button"))
 
    @pyqtSlot()
    def on_press():
        print('pressed')
        table.setItem(1,0, QTableWidgetItem("Pressed Button"))

    @pyqtSlot()
    def on_release():
        print('released')
        table.setItem(2,0, QTableWidgetItem("Released Button"))

#self.ui.pushButton.clicked.connect(self.OK)
# def OK(self):
#print 'OK pressed.'
 
# connect the signals to the slots
    btn.clicked.connect(on_click)
    btn.pressed.connect(on_press)
    btn.released.connect(on_release)


#TAB 3------------------------------
    
    # Create textbox
    textbox = QtGui.QLineEdit(tab3)
    textbox.move(20, 20)
    textbox.resize(280,40)
 
 # Create a button in the window
    button = QtGui.QPushButton('Click me', tab3)
    button.move(20,80)
 
# Create the actions
    @pyqtSlot()
    def on_click_TB():
        textbox.setText("Button clicked.")

# connect the signals to the slots
    button.clicked.connect(on_click_TB)


#TAB 4------------------------------
    # Create combobox
    combo = QtGui.QComboBox(tab4)
    combo.addItem("Python")
    combo.addItem("Perl")
    combo.addItem("Java")
    combo.addItem("C++")
    combo.move(20,20)

    # Create calendar
    cal = QtGui.QCalendarWidget(tab4)
    cal.setGridVisible(True)
    cal.move(0, 50)
    cal.resize(320,240)  


# Set title and show
    #tabs.setWindowTitle('PyQt QTabWidget @ pythonspot.com')
    tabs.show() 
# Show the window and run the app
    w.show()
    
#app.exec_()
    return sys.exit(app.exec_())


if __name__ == '__main__':
    main()
 

 


