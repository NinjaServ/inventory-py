

import pypyodbc
import pyodbc 
import datetime
import time
import plistlib

import sys
from PyQt4 import QtCore, QtGui, uic

db_str = 'C:\python35_win32\Projects\Chemical_Inventory.mdb'


##plistlib.load(fp, *, fmt=None, use_builtin_types=True, dict_type=dict)
##plistlib.dump(value, fp, *, fmt=FMT_XML, sort_keys=True, skipkeys=False)
##with open(fileName, 'rb') as fp:
##    pl = load(fp)
##print(pl["aKey"])

class ChemPull(QtGui.QWidget):
    
    def __init__(self):
        super(ChemPull, self).__init__()
        
        self.cal = QtGui.QCalendarWidget(self)
        self.initUI()
    
    
    def initUI(self):
##    # Create main menu
##        mainMenu = QtGui.QMainWindow().menuBar() # QMenuBar
##        mainMenu.setNativeMenuBar(False)
##        fileMenu = mainMenu.addMenu('File')  #&amp;
##     
##    # Add exit button  
##        exitButton = QAction(none, 'Exit', w) #QIcon('exit24.png'),
##        exitButton.setShortcut('Ctrl+Q')
##        exitButton.setStatusTip('Exit application')
##        exitButton.triggered.connect(self.close)
##        fileMenu.addAction(exitButton)
        
#        self.cal = QtGui.QCalendarWidget(self)
        self.cal.setGridVisible(True)
        self.cal.move(20, 20)
        self.cal.clicked[QtCore.QDate].connect(self.showDate)
        
        self.lbl = QtGui.QLabel(self)
        date = self.cal.selectedDate()
        self.lbl.setText(date.toString())
        self.lbl.move(130, 260)
        
        self.dbLabel = QtGui.QLabel(self)
        self.lbl.setText(db_str)
        self.lbl.move(3, 3)
        
        self.db_table = QtGui.QTableWidget(self)
        self.db_tableItem = QtGui.QTableWidgetItem()
     
        table = self.db_table    
        # initiate table
        table.setWindowTitle("QTableWidget")
        table.resize(400, 250)
        table.setRowCount(1)
        table.setColumnCount(1)
        table.move(0, 400)
        # show table
        table.show()
        
        self.yestr_btn = QtGui.QPushButton("Yesterday", self) 
#        QtGui.QPushButton('Yesterday', self)
        
        # Create a button in the window 34 
#        btn = QPushButton('Click me', w)  
        self.yestr_btn.setToolTip('Query for yesterday')
        self.yestr_btn.move(50, 200) 
        
        self.date_btn = QtGui.QPushButton("For Date", self)         
        self.date_btn.setToolTip('Query for the selected date')  
        #btn.resize(btn.sizeHint()) 
        self.date_btn.move(150, 200) 
        
        self.date_btn2 = QtGui.QPushButton("From Date", self)         
        self.date_btn2.setToolTip('Query from the selected date')  
        #btn.resize(btn.sizeHint()) 
        self.date_btn2.move(225, 200) 
        
        
        # Create the actions 
#        @pyqtSlot()
#        def on_click(): 
#        @pyqtSlot() 
#        def on_press():
#            print('pressed')
        @QtCore.pyqtSlot() 
        def on_release():      
            print('released yesterday') 
            query_string = self.yesterday()
            self.queryDate(query_string)
            
        @QtCore.pyqtSlot() 
        def on_release_for():      
            print('released 24') 
            query_string = self.queryfromDate24()
            self.queryDate(query_string)
            
        @QtCore.pyqtSlot() 
        def on_release_from():      
            print('released from') 
            query_string = self.queryfromDate()
            self.queryDate(query_string)
            
        # connect the signals to the slots 
#        btn.clicked.connect(on_click)
#        btn.pressed.connect(on_press) 
        self.yestr_btn.released.connect(on_release) 
        self.date_btn.released.connect(on_release_for) 
        self.date_btn2.released.connect(on_release_from) 

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Calendar')
        self.show()
        
        
        
    def showDate(self, date):     
    
        self.lbl.setText(date.toString())
        

#(CSS_LT.timestamp>{ts '2016-08-01 00:00:00'} And CSS_LT.timestamp<{ts '2016-08-02 00:00:00'})


    def yesterday(self):
        today = datetime.date.today()
        now = time.time()
        day_str = '%s/%s/%s' % (today.month, today.day, today.year)
        #print( today, '/  ', day_str, '-', now)
        #print ('\t', time)
        #print ('ctime:', today.ctime())
        
        yesterdate = datetime.date.today() - datetime.timedelta(days=1)
        format = "%Y-%m-%d"        
        yester_str = yesterdate.strftime(format)
        time_format = "%H:%M:%S"         
        time_str = time.strftime(time_format)
         
#        print(time_str) 
        timestamp_str = yester_str + ' 00:00:00'
#        timestamp_str = "{!s} {!s}".format(yester_str, time_str) 
#        timestamp_str = "{0} {1}".format(yester_str, time_str) 
       
        qry_string = (
            'SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,' 
            '''`Chemical Inventory`.`FT#` FROM `Chemical Inventory` WHERE (`Chemical Inventory`.`Date Pulled`>{{ts '{0}'}}) ''' 
            'ORDER BY `Chemical Inventory`.`Date Pulled`').format(timestamp_str)
        
#        print(timestamp_str)
        #print ('Yesterday    :', yester_str)   
#        print(qry_string)

        return qry_string
        
        
    def queryfromDate24(self):
#        cal = QtGui.QCalendarWidget(self)        
        _qdate = self.cal.selectedDate()
        ndate = datetime.date(_qdate.year(), _qdate.month(), _qdate.day())
        date2 = ndate + datetime.timedelta(days=1)
 
        date_format = "%Y-%m-%d"
        qdate_format = "yyyy-MM-dd"
        timestamp_str = _qdate.toString(qdate_format) + ' 00:00:00'   
        endstamp_str = date2.strftime(date_format) + ' 00:00:00'  
        
#        print(timestamp_str, ' - ', endstamp_str)
        qry_string = ('SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,'
                      '`Chemical Inventory`.`FT#` FROM `Chemical Inventory` '
                      '''WHERE (`Chemical Inventory`.`Date Pulled` between {{ts '{0}'}} AND {{ts '{1}'}}) '''
                      'ORDER BY `Chemical Inventory`.`Date Pulled`').format(timestamp_str, endstamp_str)

#'''WHERE (`Chemical Inventory`.`Date Pulled` between {ts '2016-07-16 00:00:00'} AND {ts '2016-07-17 00:00:00'}) '''
#AND `Chemical Inventory`.`Date Pulled` <= `2016-07-18`
#'''WHERE (`Chemical Inventory`.`Date Pulled` between '2016-07-16' AND '2016-07-18') '''
#'''WHERE (`Chemical Inventory`.`Date Pulled`>{ts '2016-07-16 00:00:00'}) '''
# And 'Chemical Inventory`.`Date Pulled`<{ts '2016-07-18 00:00:00'}
#From_date >= '2013-01-03' AND To_date   <= '2013-01-09'
#SELECT * FROM Product_sales WHERE From_date between '2013-01-03'AND '2013-01-09'
        
#        qry_string = ('SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,'
#                      '`Chemical Inventory`.`FT#` FROM `Chemical Inventory` '
#                      '''WHERE (`Chemical Inventory`.`Date Pulled`>{{ts '{0}'}} AND 'Chemical Inventory`.`Date Pulled`<{ts '{1}'}}) '''
#                      'ORDER BY `Chemical Inventory`.`Date Pulled`').format(timestamp_str, endstamp_str)      
        
#        print(qry_string)

        return qry_string
        
        
    def queryfromDate(self):
        _qdate = self.cal.selectedDate()
        
        qdate_format = "yyyy-MM-dd"
        timestamp_str = _qdate.toString(qdate_format) + ' 00:00:00'   
        
        qry_string = ('SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,'
                      '`Chemical Inventory`.`FT#` FROM `Chemical Inventory` '
                      '''WHERE (`Chemical Inventory`.`Date Pulled`>{{ts '{0}'}}) '''
                      'ORDER BY `Chemical Inventory`.`Date Pulled`').format(timestamp_str)
        
#        qry_string = '''SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,
#        `Chemical Inventory`.`FT#` FROM `Chemical Inventory` WHERE (`Chemical Inventory`.`Date Pulled`>%s)
#ORDER BY `Chemical Inventory`.`Date Pulled` '''.format(timestamp_str)
#        print(timestamp_str)
        
#        print(qry_string)
        
        return qry_string  
        
        
    def queryDate(self, qry_string): 
#        db_str = 'C:\python35_win32\Projects\Chemical_Inventory.mdb'

#       connection = pypyodbc.connect('Driver={Microsoft Access Driver (*.mdb)};DBQ=%s;' % \ (db_str)

        connection = pyodbc.connect('Driver={Microsoft Access Driver (*.mdb)};DBQ=%s;' % (db_str) )   
        cur = connection.cursor()
        
        print('database: %s' %(db_str))
        print('query: {0}'.format(qry_string))
        print(connection)
        print(cur)
        
        cur.execute(qry_string)
        
        table = self.db_table    
     
        # initiate table
        table.resize(400, 250)
        nRows = cur.rowcount
        nCols = len(cur.description())
        nRows = 20
        nCols = 5
        table.setRowCount(nRows)
        table.setColumnCount(nCols)
     
        # set data
        dbheaders[:], dblist[:] 
        
        for d in cur.description:
            print (d[0], end="\t\t")
            dbheaders.append()
            table.setItem(0, d, QTableWidgetItem("{}".format(d)))
        print('')

        for row in cur.fetchall():
            for field in row:
                print (field, end="\t\t")
                dblist.append()
                table.setItem(row,field, QTableWidgetItem(field))
            print ('')        
        
     
             
        #ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
        #ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
        #ui->tableWidget->resizeColumnsToContents();
        #QHeaderView::ResizeToContents to make the column wide enough to display the content, resulting in a horizontal scroll bar if necessary.
        #m_TableHeader<<"#"<<"Name"<<"Text";
        #m_pTableWidget->setHorizontalHeaderLabels(m_TableHeader);
        #m_pTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        #m_pTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        #m_pTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

##        data = {'col1':['1','2','3'], 'col2':['4','5','6'], 'col3':['7','8','9']}
##
##        horHeaders = []
##        for n, key in enumerate(sorted(self.data.keys())):
##            horHeaders.append(key)
##            for m, item in enumerate(self.data[key]):
##                newitem = QTableWidgetItem(item)
##                self.setItem(m, n, newitem)
##        self.setHorizontalHeaderLabels(horHeaders)
##
##        table.resizeColumnsToContents()
##        table.resizeRowsToContents()


        # show table
        table.show()
        
        
        #cur.commit()
        cur.close()
        connection.close()
        
#        qry_string = '''SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,
#        `Chemical Inventory`.`FT#` FROM `Chemical Inventory` WHERE (`Chemical Inventory`.`Date Pulled`>%s)
#ORDER BY `Chemical Inventory`.`Date Pulled` '''.format(timestamp_str)
#        print(timestamp_str)
        
#        cur.execute(qry_string)
#        print ('')
#        print(cur.description)
#        print ('')
#
#        for d in cur.description:
#            print (d[0], end="\t\t")
#        print ('')
#
#        for row in cur.fetchall():
#            for field in row:
#                print (field, end="\t\t")
#            print ('')
#
#        #cur.commit()
#        cur.close()
#        connection.close()
    

def main():
    
    app = QtGui.QApplication(sys.argv)
    
    cp = ChemPull()
    #ex.query()
##    ex.yesterday()
##    ex.queryfromDate24()
##    ex.queryfromDate()
    
    sys.exit(app.exec_())


#def main():
#    
#    app = QtGui.QApplication(sys.argv)
#
#    w = QtGui.QWidget()
#    w.resize(250, 150)
#    w.move(300, 300)
#    w.setWindowTitle('24 hour History')
#  
#    
#    cal = QtGui.QCalendarWidget(self)
#    cal.setGridVisible(True)
#    cal.move(20, 20)
#    cal.clicked[QtCore.QDate].connect(self.showDate)
#        
#    self.lbl = QtGui.QLabel(self)
#    date = cal.selectedDate()
#    self.lbl.setText(date.toString())
#    self.lbl.move(130, 260)
#        
#    self.setGeometry(300, 300, 350, 300)
#
#    w.show()
#
#    def showDate(self, date):     
#        self.lbl.setText(date.toString())
#    
#    
#    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
    
    










#db_str = 'C:\python35_win32\Projects\Chemical_Inventory.mdb'
#connection = pypyodbc.connect('Driver={Microsoft Access Driver (*.mdb)};DBQ=%s;' %\
#            (db_str) )
#
#print(connection)
#
#cur = connection.cursor()
#
#qry_string = '''SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Pulled`,
#`Chemical Inventory`.`FT#` FROM `Chemical Inventory` WHERE (`Chemical Inventory`.`Date Pulled`>{ts '2016-07-19 00:00:00'})
#ORDER BY `Chemical Inventory`.`Date Pulled` '''
#
##cur.execute('''SELECT * FROM 'Chemical Inventory' ''')
##cur.execute('''SELECT * FROM Chemical Inventory WHERE Chemical Name LIKE '%SiLect6000%';''')
#
###cur.execute('''SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Rec` FROM `C:\python35_win32\Projects\Chemical_Inventory`.`Chemical Inventory` `Chemical Inventory` WHERE (`Chemical Inventory`.`Date Pulled`>{ts '2016-07-15 00:00:00'}) ORDER BY `Chemical Inventory`.`Date Pulled` ''')
#cur.execute(qry_string)
#
#print ('')
#print(cur.description)
#print ('')
#
#for d in cur.description:
#    print (d[0], end="\t\t")
#print ('')
#
#for row in cur.fetchall():
#    for field in row:
#        print (field, end="\t\t")
#    print ('')
#
##SELECT `Chemical Inventory`.`Chemical Name`, `Chemical Inventory`.`Date Rec`, `Chemical Inventory`.`Date Pulled`, `Chemical Inventory`.`Exp Date`, `Chemical Inventory`.`FT#`, `Chemical Inventory`.`Lot Number`, `Chemical Inventory`.CSS
##FROM `C:\python35_win32\Projects\Chemical_Inventory`.`Chemical Inventory` `Chemical Inventory`
##WHERE (`Chemical Inventory`.`Date Pulled`>{ts '2016-07-01 00:00:00'})
##ORDER BY `Chemical Inventory`.`Date Pulled`
#
##cur.commit()
#
#
#cur.close()
#connection.close()


##import sys
##from PyQt4.QtGui import *
##from PyQt4.QtCore import *
##from PyQt4.QtSql import *
##from TreeGui import Ui_MainWindow
##
##class MyForm(QMainWindow):
##def _init_(self):
##QWidget._init_(self)
###Assigning ui From Qt Designer 
###Class Name in Qt Gui
##self.ui=Ui_MainWindow()
##self.ui.setupUi(self)
##self.setWindowTitle("Table Model Sample")
##
###Connect to Database
##db=QSqlDatabase.addDatabase("QMYSQL")
##db.setHostName("localhost")
##db.setDatabaseName("nmac")
##db.setUserName("cho")
##db.setPassword("cho")
##if(db.open()):
##
###Displying Data from MySql Db inTree View
##tablemodel=QSqlQueryModel()
##tablemodel.setQuery("select * from sfo")
##self.ui.tableView.setModel(tablemodel) 



##if __name__=="__main__":
##app=QApplication(sys.argv)
##myui=MyForm()
##myui.show()
##sys.exit(app.exec_())
